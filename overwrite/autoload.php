<?php
spl_autoload_register(function ($cls) {
    $classMap = [
        'think\\Template' => __DIR__ . '/think/Template.php',
    ];

    if (isset($classMap[$cls])) {
        include $classMap[$cls];
        return true;
    }
// 注意需要设置prepend参数为true
}, true, true);
