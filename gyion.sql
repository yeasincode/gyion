SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gy_category
-- ----------------------------
DROP TABLE IF EXISTS `gy_category`;
CREATE TABLE `gy_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `belong` varchar(20) DEFAULT NULL COMMENT '分类所属',
  `level1` varchar(30) DEFAULT NULL COMMENT '一级分类',
  `level_order1` int(11) DEFAULT NULL COMMENT '一级分类排序',
  `level2` varchar(30) DEFAULT NULL COMMENT '二级分类',
  `level_order2` int(11) DEFAULT NULL COMMENT '二级分类排序',
  `level3` varchar(30) DEFAULT NULL COMMENT '三级分类',
  `level_order3` int(11) DEFAULT NULL COMMENT '三级分类排序',
  `level4` varchar(30) DEFAULT NULL COMMENT '四级分类',
  `level_order4` int(11) DEFAULT NULL COMMENT '四级分类排序',
  `level5` varchar(30) DEFAULT NULL COMMENT '五级分类',
  `level_order5` int(11) DEFAULT NULL COMMENT '五级分类排序',
  `create_user` int(11) NOT NULL COMMENT '创建人',
  `create_name` varchar(20) NOT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_user` int(11) DEFAULT NULL COMMENT '修改人',
  `modify_name` varchar(20) DEFAULT NULL COMMENT '修改人名称',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `parent_id` int(11) NOT NULL COMMENT '父分类id',
  PRIMARY KEY (`id`),
  KEY `create_usertime_ix` (`create_time`) USING BTREE,
  KEY `modify_usertime_ix` (`modify_time`) USING BTREE,
  KEY `category_create_user_fk` (`create_user`),
  KEY `category_modify_user_fk` (`modify_user`),
  KEY `category_parent_id_fk` (`parent_id`),
  CONSTRAINT `category_create_user_fk` FOREIGN KEY (`create_user`) REFERENCES `gy_user` (`id`),
  CONSTRAINT `category_modify_user_fk` FOREIGN KEY (`modify_user`) REFERENCES `gy_user` (`id`),
  CONSTRAINT `category_parent_id_fk` FOREIGN KEY (`parent_id`) REFERENCES `gy_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类';

-- ----------------------------
-- Table structure for gy_content
-- ----------------------------
DROP TABLE IF EXISTS `gy_content`;
CREATE TABLE `gy_content` (
  `id` int(11) NOT NULL COMMENT '内容id',
  `content_url` varchar(200) DEFAULT NULL COMMENT '内容路径',
  `content_pic_url` varchar(200) DEFAULT NULL COMMENT '内容图像路径',
  `create_user` int(11) NOT NULL COMMENT '创建人',
  `create_name` varchar(20) NOT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_user` int(11) DEFAULT NULL COMMENT '修改人',
  `modify_name` varchar(20) DEFAULT NULL COMMENT '修改人名称',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `content_create_user_fk` (`create_user`),
  KEY `content_modify_user_fk` (`modify_user`),
  KEY `create_usertime_ix` (`create_time`) USING BTREE,
  KEY `modify_usertime_ix` (`modify_time`) USING BTREE,
  CONSTRAINT `content_create_user_fk` FOREIGN KEY (`create_user`) REFERENCES `gy_user` (`id`),
  CONSTRAINT `content_modify_user_fk` FOREIGN KEY (`modify_user`) REFERENCES `gy_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='内容';

-- ----------------------------
-- Table structure for gy_fileinfo
-- ----------------------------
DROP TABLE IF EXISTS `gy_fileinfo`;
CREATE TABLE `gy_fileinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文件id',
  `filename` varchar(100) DEFAULT NULL COMMENT '文件名',
  `realname` varchar(100) DEFAULT NULL COMMENT '文件真名',
  `protocol` varchar(20) DEFAULT NULL COMMENT '访问文件协议',
  `url` varchar(200) DEFAULT NULL COMMENT '访问路径',
  `ext` varchar(10) DEFAULT NULL COMMENT '文件后缀',
  `entity_type` varchar(20) DEFAULT NULL COMMENT '实体类型',
  `entity_id` int(11) DEFAULT NULL COMMENT '实体id',
  `create_user` int(11) NOT NULL COMMENT '创建人',
  `create_name` varchar(20) NOT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_user` int(11) DEFAULT NULL COMMENT '修改人',
  `modify_name` varchar(20) DEFAULT NULL COMMENT '修改人名称',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `modify_usertime_ix` (`modify_time`) USING BTREE,
  KEY `create_usertime_ix` (`create_time`) USING BTREE,
  KEY `file_create_user_fk` (`create_user`),
  KEY `file_modify_user_fk` (`modify_user`),
  CONSTRAINT `file_create_user_fk` FOREIGN KEY (`create_user`) REFERENCES `gy_user` (`id`),
  CONSTRAINT `file_modify_user_fk` FOREIGN KEY (`modify_user`) REFERENCES `gy_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gy_menu
-- ----------------------------
DROP TABLE IF EXISTS `gy_menu`;
CREATE TABLE `gy_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `menuname` varchar(20) NOT NULL COMMENT '菜单名',
  `menu_icon` varchar(20) DEFAULT NULL COMMENT '菜单图标',
  `menu_url` varchar(100) NOT NULL COMMENT '菜单相对路径',
  `menu_module` varchar(20) NOT NULL COMMENT '菜单模块',
  `create_user` int(11) NOT NULL COMMENT '创建人',
  `create_name` varchar(20) NOT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_user` int(11) DEFAULT NULL COMMENT '修改人',
  `modify_name` varchar(20) DEFAULT NULL COMMENT '修改人名称',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `menu_create_user_fk` (`create_user`),
  KEY `menu_modify_user_fk` (`modify_user`),
  KEY `create_usertime_ix` (`create_time`) USING BTREE,
  KEY `modify_usertime_ix` (`modify_time`) USING BTREE,
  CONSTRAINT `menu_create_user_fk` FOREIGN KEY (`create_user`) REFERENCES `gy_user` (`id`),
  CONSTRAINT `menu_modify_user_fk` FOREIGN KEY (`modify_user`) REFERENCES `gy_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单';

-- ----------------------------
-- Table structure for gy_permission
-- ----------------------------
DROP TABLE IF EXISTS `gy_permission`;
CREATE TABLE `gy_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `pername` varchar(20) DEFAULT NULL COMMENT '权限名',
  `entity_id` int(11) DEFAULT NULL COMMENT '实体id',
  `entity_type` varchar(20) DEFAULT NULL COMMENT '实体类型',
  `entity_name` varchar(20) DEFAULT NULL COMMENT '实体名称',
  `entity_value` varchar(100) DEFAULT NULL COMMENT '实体取值',
  `create_user` int(11) NOT NULL COMMENT '创建人',
  `create_name` varchar(20) NOT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_user` int(11) DEFAULT NULL COMMENT '修改人',
  `modify_name` varchar(20) DEFAULT NULL COMMENT '修改人名称',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `create_usertime_ix` (`create_time`) USING BTREE,
  KEY `modify_usertime_ix` (`modify_time`) USING BTREE,
  KEY `per_create_user_fk` (`create_user`),
  KEY `per_modify_user_fk` (`modify_user`),
  CONSTRAINT `per_create_user_fk` FOREIGN KEY (`create_user`) REFERENCES `gy_user` (`id`),
  CONSTRAINT `per_modify_user_fk` FOREIGN KEY (`modify_user`) REFERENCES `gy_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gy_role
-- ----------------------------
DROP TABLE IF EXISTS `gy_role`;
CREATE TABLE `gy_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(20) DEFAULT NULL COMMENT '角色名',
  `create_user` int(11) NOT NULL COMMENT '创建人',
  `create_name` varchar(50) NOT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_user` int(11) DEFAULT NULL COMMENT '修改人',
  `modify_name` varchar(50) DEFAULT NULL COMMENT '修改人名称',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `create_usertime_ix` (`create_time`) USING BTREE,
  KEY `modify_usertime_ix` (`modify_time`) USING BTREE,
  KEY `role_create_user_fk` (`create_user`),
  KEY `role_modify_user_fk` (`modify_user`),
  CONSTRAINT `role_create_user_fk` FOREIGN KEY (`create_user`) REFERENCES `gy_user` (`id`),
  CONSTRAINT `role_modify_user_fk` FOREIGN KEY (`modify_user`) REFERENCES `gy_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gy_role_per
-- ----------------------------
DROP TABLE IF EXISTS `gy_role_per`;
CREATE TABLE `gy_role_per` (
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `per_id` int(11) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`role_id`,`per_id`),
  KEY `rp_per_fk` (`per_id`),
  CONSTRAINT `rp_per_fk` FOREIGN KEY (`per_id`) REFERENCES `gy_permission` (`id`),
  CONSTRAINT `rp_role_fk` FOREIGN KEY (`role_id`) REFERENCES `gy_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限关系表';

-- ----------------------------
-- Table structure for gy_setting
-- ----------------------------
DROP TABLE IF EXISTS `gy_setting`;
CREATE TABLE `gy_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '设置id',
  `setting_name` varchar(50) DEFAULT NULL COMMENT '设置名',
  `setting_code` varchar(50) DEFAULT NULL COMMENT '设置编码',
  `setting_value` varchar(100) DEFAULT NULL COMMENT '设置取值',
  `create_user` int(11) NOT NULL COMMENT '创建人',
  `create_name` varchar(50) NOT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_user` int(11) DEFAULT NULL COMMENT '修改人',
  `modify_name` varchar(50) DEFAULT NULL COMMENT '修改人名称',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `create_usertime_ix` (`create_time`) USING BTREE,
  KEY `modify_usertime_ix` (`modify_time`) USING BTREE,
  KEY `setting_create_user_fk` (`create_user`),
  KEY `setting_modify_user_fk` (`modify_user`),
  CONSTRAINT `setting_create_user_fk` FOREIGN KEY (`create_user`) REFERENCES `gy_user` (`id`),
  CONSTRAINT `setting_modify_user_fk` FOREIGN KEY (`modify_user`) REFERENCES `gy_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设置表';

-- ----------------------------
-- Table structure for gy_user
-- ----------------------------
DROP TABLE IF EXISTS `gy_user`;
CREATE TABLE `gy_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户Id',
  `username` varchar(20) NOT NULL COMMENT '用户账号',
  `password` char(32) NOT NULL COMMENT '用户密码',
  `realname` varchar(20) DEFAULT NULL COMMENT '用户姓名',
  `nickname` varchar(20) DEFAULT NULL COMMENT '用户昵称',
  `phone` varchar(20) DEFAULT NULL COMMENT '用户手机',
  `birthdate` date DEFAULT NULL COMMENT '用户生日',
  `user_status` varchar(10) DEFAULT NULL COMMENT '用户状态',
  `role_name` varchar(20) DEFAULT NOT NULL COMMENT '角色名',
  `role_id` int(11) DEFAULT NOT NULL COMMENT '角色id',
  `create_user` int(11) NOT NULL COMMENT '创建人',
  `create_name` varchar(50) NOT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_user` int(11) DEFAULT NULL COMMENT '修改人',
  `modify_name` varchar(50) DEFAULT NULL COMMENT '修改人名称',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `create_user_fk` (`create_user`),
  KEY `modify_user_fk` (`modify_user`),
  KEY `create_usertime_ix` (`create_time`) USING BTREE,
  KEY `modify_usertime_ix` (`modify_time`) USING BTREE,
  KEY `role_fk` (`role_id`),
  CONSTRAINT `create_user_fk` FOREIGN KEY (`create_user`) REFERENCES `gy_user` (`id`),
  CONSTRAINT `modify_user_fk` FOREIGN KEY (`modify_user`) REFERENCES `gy_user` (`id`),
  CONSTRAINT `role_fk` FOREIGN KEY (`role_id`) REFERENCES `gy_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Table structure for gy_lang
-- ----------------------------
DROP TABLE IF EXISTS `gy_lang`;
CREATE TABLE `gy_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '语言Id',
  `lang_code` varchar(20) NOT NULL COMMENT '语言代码',
	`lang_name` varchar(10) NOT NULL COMMENT '语言名称',
  `create_user` int(11) NOT NULL COMMENT '创建人',
  `create_name` varchar(50) NOT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_user` int(11) DEFAULT NULL COMMENT '修改人',
  `modify_name` varchar(50) DEFAULT NULL COMMENT '修改人名称',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `create_user_fk` (`create_user`),
  KEY `modify_user_fk` (`modify_user`),
  KEY `create_usertime_ix` (`create_time`) USING BTREE,
  KEY `modify_usertime_ix` (`modify_time`) USING BTREE,
  CONSTRAINT `lang_create_user_fk` FOREIGN KEY (`create_user`) REFERENCES `gy_user` (`id`),
  CONSTRAINT `lang_modify_user_fk` FOREIGN KEY (`modify_user`) REFERENCES `gy_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='语言';

-- ----------------------------
-- Table structure for gy_text
-- ----------------------------
DROP TABLE IF EXISTS `gy_text`;
CREATE TABLE `gy_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文本Id',
  `lang_id` int(11) NOT NULL COMMENT '语言Id',
  `lang_code` varchar(20) NOT NULL COMMENT '语言代码',
  `text_code` varchar(20) NOT NULL COMMENT '文本编码',
  `text` varchar(200) NOT NULL COMMENT '文本',
  `text_group` varchar(20)  NULL COMMENT '文本分组',
  `text_comment` varchar(200)  NULL COMMENT '文本备注',
  `create_user` int(11) NOT NULL COMMENT '创建人',
  `create_name` varchar(50) NOT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_user` int(11) DEFAULT NULL COMMENT '修改人',
  `modify_name` varchar(50) DEFAULT NULL COMMENT '修改人名称',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `create_user_fk` (`create_user`),
  KEY `modify_user_fk` (`modify_user`),
  KEY `create_usertime_ix` (`create_time`) USING BTREE,
  KEY `modify_usertime_ix` (`modify_time`) USING BTREE,
  KEY `lang_fk` (`lang_id`),
  CONSTRAINT `text_create_user_fk` FOREIGN KEY (`create_user`) REFERENCES `gy_user` (`id`),
  CONSTRAINT `text_modify_user_fk` FOREIGN KEY (`modify_user`) REFERENCES `gy_user` (`id`),
  CONSTRAINT `lang_fk` FOREIGN KEY (`lang_id`) REFERENCES `gy_lang` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='文本';

-- ----------------------------
-- Records
-- ----------------------------
BEGIN;
INSERT INTO `gy_role`(`id`, `role_name`, `create_user`, `create_name`, `create_time`, `modify_user`, `modify_name`, `modify_time`) VALUES (1, '系统管理员', 1, 'system', '2019-03-17 17:00:51', NULL, NULL, NULL);
/**user*/
INSERT INTO `gy_user`(`id`, `username`, `password`, `realname`, `nickname`, `phone`, `birthdate`, `user_status`, `role_name`, `role_id`, `create_user`, `create_name`, `create_time`, `modify_user`, `modify_name`, `modify_time`) VALUES (1, 'system', '123456', '系统', NULL, '', '2019-03-17', '已启用', '系统管理员', 1, 1, 'system', '2019-02-24 11:16:57', 1, 'system', '2019-03-17 17:02:34');
INSERT INTO `gy_user`(`id`, `username`, `password`, `realname`, `nickname`, `phone`, `birthdate`, `user_status`, `role_name`, `role_id`, `create_user`, `create_name`, `create_time`, `modify_user`, `modify_name`, `modify_time`) VALUES (2, 'system', '123456', '管理员', NULL, '', '2019-03-17', '已启用', '超级管理员', 1, 1, 'system', '2019-02-24 11:16:57', 1, 'system', '2019-03-17 17:02:34');
/**lang*/
INSERT INTO `gy_lang` VALUES (1, 'zh-cn', '中文', 1,'system', '2019-03-16 11:16:57', 1, 'system', '2019-02-24 11:17:26');
INSERT INTO `gy_lang` VALUES (2, 'en-us', '英文', 2,'system', '2019-03-16 11:16:57', 1, 'system', '2019-02-24 11:17:26');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
