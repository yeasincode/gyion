<?php
namespace gyion\service;

use gyion\core\S;

class UserService extends S
{
    protected function repositorys(){
        return ['user'];
    }

    /**
     * 查询用户列表
     * @param $query 查询对象
     */
    public function getList($queryInfo=null,$pageInfo=null)
    {
        return $this->RUser()->list($queryInfo,$pageInfo);
    }

    /**
     * 检查用户是否存在
     * @param $username 用户名
     * @param $password 密码
     */
    public function checkUser($username, $password)
    {
        $user = $this->RUser()->getByName($username);
        if (\strcmp($user['password'], $password) == 0) {
            return true;
        }
        return false;
    }

    /**
     * 更改用户状态
     */
    public function changeStatus($userId,$userStatus){
        $this->RUser()->update($userId,['user_status'=>$userStatus]);
    }

    /**
     * 删除用户
     * @param $userId 用户id
     */
    public function delete($userId){
        $this->RUser()->delete($userId);
    }
}
