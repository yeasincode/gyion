var common = {};
/**
 * 初始化
 * @param initList 初始化列表 date query
 */
common.init = function (initList) {
    /**
     * 初始化日期
     */
    init_date = function () {
        /**初始化日期 */
        layui.use('laydate', function () {
            var laydate = layui.laydate;
            $("input[g-type='date']").each(function (index, elem) {
                laydate.render({
                    elem: elem
                });
            });
        });

    }

    init_query = function () {
        /**
         * 初始化表单
         */
        layui.use('form', function () {
            var form = layui.form;
            form.on('submit(sreach)', function (data) {
                buildQueryModel(data.form);
            });
        });
    }
    for (var i in initList) {
        var item = initList[i];
        var func = "init_" + item + "()";
        eval(func);
    }
}

/**
 * 改变Url
 * @param {url} url 
 * @param {参数名} arg 
 * @param {参数值} arg_val 
 */
function changeURLArg(url, arg, arg_val) {
    var pattern = arg + '=([^&]*)';
    var replaceText = arg + '=' + arg_val;
    if (url.match(pattern)) {
        var tmp = '/(' + arg + '=)([^&]*)/gi';
        tmp = url.replace(eval(tmp), replaceText);
        return tmp;
    } else {
        if (url.match('[\?]')) {
            return url + '&' + replaceText;
        } else {
            return url + '?' + replaceText;
        }
    }
}

/**
 * 返回查询对象
 * @param {form} form 
 */
function buildQueryModel(form) {

    var queryModel = {
        fields: [],
        queryStr: ''
    };

    for (var i = 0; i < form.length; i++) {
        var input = $(form[i]);
        if (input.attr('name') === 'queryStr') {
            queryModel.queryStr = input.val();
            continue;
        }

        var field = {
            fieldType: input.attr('g-type'),
            fieldModel: input.attr('g-model'),
            fieldName: input.attr('name'),
            fieldValue: input.val()
        };

        if (input.attr('g-group')) {
            var groupVal = [input.val()];
            for (var j = i + 1; j < form.length; j++) {
                var input_next = $(form[j]);
                if (input_next.attr('g-group') === input.attr('g-group')) {
                    groupVal.push(input_next.val());
                    continue;
                }
                //抵消i++所增的步数
                i = j-1;
                field.fieldValue = groupVal;
                break;
            }
        }
        if (field.fieldType) {
            queryModel.fields.push(field);
        }
    }

    $(form).find("input[name='queryModel']").val(JSON.stringify(queryModel));
}


/**
 * 分页回调
 * @param {页码} page 
 */
function pageChange(current) {
    location.href = changeURLArg(location.href, 'current', current);
}