<?php
namespace gyion\platform\controller;

use gyion\core\web\C;

class Index extends C
{
    protected function services()
    {
        return ['user'];
    }

    public function index()
    {
        return view();
    }

    public function testQueryModel()
    {
        if ($this->request->isGet()) {
            return view();
        }

        $query = $this->request->queryModel();
        $result = $this->SUser()->getUserList($query);
    }

}
