<?php
namespace gyion\platform\controller;

use gyion\core\web\C;

/**
 * 角色控制器
 */
class Role extends C
{
    protected function services()
    {
        return ['role'];
    }

    /**
     * 角色列表
     */
    public function role_list()
    {
        return view();
    }

}
