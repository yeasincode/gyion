<?php
namespace gyion\platform\controller;

use gyion\core\web\C;
use think\Session;
use gyion\core\data\ViewData;

class Login extends C
{

    protected function services()
    {
        return ['user'];
    }

    public function index($username = null, $password = null)
    {
        if (request()->isGet()) {
            return \view_none('index');
        }

        $validate = validate('User');
        if (!$validate->check([
            'name' => $username,
            'password' => $password,
        ])) {
            return \view_error('index',$validate->getError());
        }

        $isSuccess = $this->SUser()->checkUser($username, $password);

        if (!$isSuccess) {
            return \view_error('index');
        }
        
        Session::set('user', $username);
        return \mredirect('index');
    }
}
