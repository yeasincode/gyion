<?php
namespace gyion\platform\controller;

use gyion\core\web\C;

/**
 * 用户控制器
 */
class User extends C
{
    protected function services()
    {
        return ['user'];
    }

    //--------------页面-----------------
    /**
     * 管理员列表
     */
    public function admin_list()
    {
        $queryInfo = $this->request->queryInfo();
        $pageInfo = $this->request->pageInfo();

        $list = $this->SUser()->getList($queryInfo, $pageInfo);
        return view_success('admin_list', [
            'data' => $list,
            'queryInfo' => $queryInfo,
            'queryComponent' => 'User',
            'pageInfo' => $pageInfo,
        ]);
    }

    /**
     * 添加用户
     */
    public function admin_add(){
        return view();
    }

    //--------------Ajax接口-----------------
    /**
     * 修改用户状态
     * @param $userId 用户id
     * @param $userStatus 用户状态
     */
    public function change_status($userId,$userStatus)
    {
        $this->SUser()->changeStatus($userId, $userStatus);
        return \json_success('修改用户状态成功！');
    }

    /**
     * 删除用户
     * @param $userId 用户Id
     */
    public function del_user($userId)
    {
        $this->SUser()->delete($userId);
        return \json_success('删除用户成功！');
    }
}
