<?php
//模块公共文件
use gyion\core\db\DateField;
use gyion\core\db\Field;
use gyion\core\db\P;
use gyion\core\db\Q;
use think\Request;

/**
 * 构造查询对象
 */
function queryInfo(Request $request)
{
    $jsonStr = $request->post('queryModel');

    if (empty($jsonStr)) {
        return null;
    }
    $query = json_decode($jsonStr, true);
    $fieldList = [];
    $queryStr = '';
    foreach ($query['fields'] as $item) {
        if (strcmp($item['fieldType'], 'date') == 0) {
            $values = $item['fieldValue'];
            $startValue = array_shift($values);
            $endValue = array_shift($values);
            $field = new DateField($item['fieldName'], $item['fieldType'], $item['fieldModel'], $startValue, $endValue);
            array_push($fieldList, $field);
        } else {
            $field = new Field($item['fieldName'], $item['fieldValue'], $item['fieldType'], $item['fieldModel']);
            array_push($fieldList, $field);
        }
    }

    if (isset($query['queryStr'])) {
        $queryStr = $query['queryStr'];
    }
    $queryModel = new Q($fieldList, $queryStr);
    return $queryModel;
}

/**
 * 构造分页对象
 */
function pageInfo(Request $request)
{
    $pageInfo = new P();
    $fields =['current','pageSize','totalSize','orders'];
    $count = 0;
    foreach ($fields as $key) {
        $getValue = $request->get($key);
        if (isset($getValue)) {
            $pageInfo->$key($getValue);
            $count++;
        }
    }
    return $pageInfo;
}
