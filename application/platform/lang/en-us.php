<?php
return [
    'login_header'=>'system login',
    'login'=>'login',
    'username'=>'user name',
    'password'=>'password',
    'index_header'=>'management',
    'index_left_open'=>'open left side'
];