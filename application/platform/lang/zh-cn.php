<?php
return [
    'login_header'=>'系统登录',
    'login'=>'登录',
    'username'=>'用户名',
    'password'=>'密码',
    'index_header'=>'后台管理',
    'index_left_open'=>'展开侧边栏'
];