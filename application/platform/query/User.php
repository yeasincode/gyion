<?php
namespace gyion\platform\query;

use gyion\core\web\query\Query;

class User extends Query
{
    public function init()
    {
        $this->add([ [
            'name' => 'create_time',
            'model' => 'user',
            'group' => 'user',
            'placeholder' => '开始日期-结束日期',
            'type' => 'date',
        ],[
            'name' => 'queryStr', //特殊意义Url传输
            'placeholder' => '用户名',
            'type' => 'text',
        ]]);
    }
}
