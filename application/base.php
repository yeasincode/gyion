<?php
define('RESOURCE_PATH', ROOT_PATH . 'resource' . DS);
define('APP_CORE_PATH', APP_PATH . 'core' . DS);
define('APP_REPOSITORY_PATH', APP_PATH . 'repository' . DS);
define('APP_SERVICE_PATH', APP_PATH . 'service' . DS);
define('APP_PLATFORM_PATH', APP_PATH . 'platform' . DS);
define('APP_NAMESPACE', 'gyion');
/**
 * 获取两个key唯一hash
 * @param $key1 键1
 * @param $key2 键2
 * @return hash code
 */
function hash2key($key1, $key2)
{
    $keys = [$key1, $key2];
    sort($keys, SORT_STRING);
    $hash = hash('md4', $keys[0] . $keys[1]);
    return $hash;
}
