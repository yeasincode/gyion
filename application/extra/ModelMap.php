<?php
use gyion\core\db\TableMap;

return [
    "Table" => [
        "user" => [
            "alias" => "u",
        ],
        "role" => [
            "alias" => "r",
        ],
        "content" => [
            "alias" => "c",
        ],
        "fileinfo" => [
            "alias" => "fi",
        ],
        "menu" => [
            "alias" => "m",
        ],
        "permission" => [
            "alias" => "p",
        ],
    ],
    "Relation" => [
        hash2key('user', 'role') => [
            'table1' => 'user',
            'table2' => 'role',
            'join' => 'u.role_id=r.id',
        ],
        hash2key('content', 'fileinfo') => [
            'table1' => 'content',
            'table2' => 'fileinfo',
            'joinType' => 'inner join',
            'join' => 'c.id=fi.entity_id and fi.entity_type="file"',
        ],
        hash2key('menu', 'permission') => [
            'table1' => 'menu',
            'table2' => 'permission',
            'joinType' => 'inner join',
            'join' => 'm.id=p.entity_id and p.entity_type="menu"',
        ]],
];
