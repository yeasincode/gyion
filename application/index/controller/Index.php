<?php
namespace gyion\index\controller;
use gyion\core\web\C;
use gyion\service\UserService;
class Index extends C
{

    protected function services(){
        return ['user'];
    }

    public function index()
    {
        $userList=$this->SUser()->getUserList();
        return view('index',$userList[0]);
    }
}
