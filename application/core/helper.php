<?php
use gyion\core\web\data\ViewData;
use gyion\core\web\data\RestData;
use gyion\core\web\V;
use think\Config;
use think\Lang;
use think\Request;
use think\Response;
use think\Url;
/**
 * 渲染模板输出
 * @param string    $template 模板文件
 * @param array     $vars 模板变量
 * @param array     $replace 模板替换
 * @param integer   $code 状态码
 * @return \gyion\core\View
 */
function view($template = '', $vars = [], $replace = [], $code = 200)
{
    $request = Request::instance();
    $v = $template;
    if (strcmp($request->module(), Config::get('default_module')) == 0) {

        $skin = 'default';
        $app_view_path = Config::get('template')['app_view_path'];
        $view_suffix = Config::get('template')['view_suffix'];
        if (is_bool(\strpos($template, DS))) {
            $v = $app_view_path . $skin . DS . $v . '.' . $view_suffix;
        }
    }
    $header = [];
    $options = [];

    $view = new V($v, $code, $header, $options);
    $view->replace($replace)->assign($vars);
    return $view;
}

/**
 * 页面数据统一封装
 * @param object $data 数据
 * @param string $msg 提示消息
 * @param string $status 状态
 * @return gyion\core\data\ViewData
 */
function view_data($data = null, $msg = null, $status = 'none')
{
    $classMap = [
        ViewData::$status_error => 'gyion\\core\\web\\data\\Error',
        ViewData::$status_success => 'gyion\\core\\web\\data\\Success',
    ];

    if (!array_key_exists($status, $classMap)) {
        $msg = $msg == null ? Lang::get('request_success') : $msg;
        return new ViewData();
    }
    $class = $classMap[$status];

    if (array_key_exists('data', $data)) {
        echo 'todo';
        $dataVal = $data['data'];
        $pageInfo = $data['pageInfo'];
        $queryInfo = $data['queryInfo'];
        $queryComponent = $data['queryComponent'];
        $_data = new $class($msg, $dataVal);
        return new ViewData($_data, $queryInfo, $pageInfo, $queryComponent, $status);
    }

    $_data = new $class($msg, $data);
    return new ViewData($_data, null, null,null, $status);
}

/**
 * 渲染模板输出 并且返回空视图数据
 * @param string    $template 模板文件
 * @param array     $vars 模板变量
 * @param array     $replace 模板替换
 * @param integer   $code 状态码
 * @return \gyion\core\View
 */
function view_none($template = '', $vars = [], $replace = [], $code = 200)
{
    $data = view_data($vars, Lang::get('request_success'), 'none');
    return view($template, ['ViewData' => $data], $replace, $code);
}

/**
 * 渲染模板输出 并且返回成功状态视图数据
 * @param string    $template 模板文件
 * @param array     $vars 模板变量
 * @param array     $replace 模板替换
 * @param integer   $code 状态码
 * @return \gyion\core\View
 */
function view_success($template = '', $vars = [], $msg = null, $replace = [], $code = 200)
{
    $msg = $msg == null ? Lang::get('request_success') : $msg;
    $data = view_data($vars, $msg, 'success');
    return view($template, ['ViewData' => $data], $replace, $code);
}

/**
 * 渲染模板输出 并且返回错误状态视图数据
 * @param string    $template 模板文件
 * @param array     $vars 模板变量
 * @param array     $replace 模板替换
 * @param integer   $code 状态码
 * @return \gyion\core\View
 */
function view_error($template = '', $vars = [], $msg = null, $replace = [], $code = 200)
{
    $msg = $msg == null ? Lang::get('request_faild') : $msg;
    $data = view_data($vars, $msg, 'error');
    return view($template, ['ViewData' => $data], $replace, $code);
}

/**
 * 获取\think\response\Json对象实例
 * @param mixed   $data 返回的数据
 * @param integer $code 状态码
 * @param array   $header 头部
 * @param array   $options 参数
 * @return \think\response\Json
 */
function json($data = [], $code = 200, $header = [], $options = [])
{
    return Response::create($data, 'json', $code, $header, $options);
}

/**
 * 获取\think\response\Json对象实例 并返回成功 RestData数据
 * @param mixed   $data 返回的数据
 * @param string  $msg 消息
 * @param integer $code 状态码
 * @param array   $header 头部
 * @param array   $options 参数
 * @return \think\response\Json
 */
function json_success($data = [],$msg=null, $code = 200, $header = [], $options = [])
{
    $msg = $msg == null ? Lang::get('request_success') : $msg;
    $jsonData=new RestData($data,$msg,true);
    return json($jsonData,$code,$header,$options);
}

/**
 * 获取\think\response\Json对象实例 并返回失败 RestData数据
 * @param mixed   $data 返回的数据
 * @param string  $msg 消息
 * @param integer $code 状态码
 * @param array   $header 头部
 * @param array   $options 参数
 * @return \think\response\Json
 */
function json_error($data = [], $msg=null,$code = 200, $header = [], $options = [])
{
    $msg = $msg == null ? Lang::get('request_faild') : $msg;
    $jsonData=new RestData($data,$msg,false);
    return json($jsonData,$code,$header,$options);
}

/**
 * 模块Url生成
 * @param string        $url 路由地址
 * @param string|array  $vars 变量
 * @param bool|string   $suffix 生成的URL后缀
 * @param bool|string   $domain 域名
 * @return string
 */
function murl($url = '', $vars = '', $suffix = true, $domain = false)
{
    if (!strpos($url, '\/')) {
        $url = '@/' . request()->module() . '/' . $url;
    }
    return Url::build($url, $vars, $suffix, $domain);
}

/**
 * 获取\think\response\Redirect对象实例
 * @param mixed         $url 重定向地址 支持Url::build方法的地址
 * @param array|integer $params 额外参数
 * @param integer       $code 状态码
 * @param array         $with 隐式传参
 * @return \think\response\Redirect
 */
function mredirect($url = [], $params = [], $code = 302, $with = [])
{
    return redirect(murl($url), $params, $code, $with);
}
