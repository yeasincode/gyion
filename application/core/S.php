<?php
namespace gyion\core;

use gyion\core\db\R;

/**
 * 服务层基类
 */
class S
{
    /**
     * 实例缓存
     */
    static $_cache = [];

    /**
     * 服务列表
     */
    protected function services()
    {
        return [];
    }

    /**
     * 仓储列表
     */
    protected function repositorys()
    {
        return [];
    }

    /**
     * S{Service},R{Repository}方法生成
     */
    public function __call($funcName, $arguments)
    {
        $type = $funcName[0];
        switch ($type) {
            case 'S':
                return $this->serviceInstance($funcName);
            case 'R':
                return $this->repositoryInstance($funcName);
        }

        return null;
    }
    /**
     * 仓储实例
     */
    private function repositoryInstance($funcName)
    {
        foreach ($this->repositorys() as $name) {
            $_name = ucfirst(strtolower($name));
            $repositoryName = 'R' . $_name;
            $instance=null;
            if(array_key_exists($repositoryName,R::$_cache)){
                $instance = R::$_cache[$repositoryName];
                return $instance;
            }
            if (strcmp($repositoryName, $funcName) == 0) {
                include APP_REPOSITORY_PATH . $_name . 'Repository' . EXT;
                $class = 'gyion\\repository\\' . $_name. 'Repository';
                $instance = new $class();
                R::$_cache[$repositoryName] = $instance;
                return $instance;
            }
        }
        return null;
    }


    /**
     * 服务实例
     */
    private function serviceInstance($funcName)
    {
        foreach ($this->services() as $name) {
            $_name = ucfirst(strtolower($name));
            $serviceName = 'S' . $_name;
            $instance=null;
            if(array_key_exists($serviceName,R::$_cache)){
                $instance = R::$_cache[$serviceName];
                return $instance;
            }
            if (strcmp($serviceName, $funcName) == 0) {
                include APP_SERVICE_PATH . $_name . 'Service' . EXT;
                $class = 'gyion\\service\\' . $_name. 'Service';
                $instance = new $class();
                R::$_cache[$serviceName] = $instance;
                return $instance;
            }
        }
        return null;
    }
}
