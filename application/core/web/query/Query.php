<?php
namespace gyion\core\web\query;

/**
 * 页面查询构造
 */
class Query
{
    protected $items = [];

    /**
     * 初始化
     */
    public function init()
    {

    }

    /**
     * 添加
     */
    protected function add($vars = [])
    {
        foreach ($vars as $var_item) {
            $item = \array_object($var_item, 'gyion\core\web\query\QueryItem');
            array_push($this->items, $item);
        }
    }

    /**
     * 获得查询项列表
     */
    public function items()
    {
        return $this->items;
    }

}
