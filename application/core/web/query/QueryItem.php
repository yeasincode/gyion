<?php
namespace gyion\core\web\query;

/**
 * 查询对象
 */
class QueryItem
{
    private $name;
    private $model;
    private $type;
    private $group;
    private $placeholder;
    private $required;

    public function __construct()
    {
        $this->type = 'text';
        $this->required = false;
    }

    public function name($name = null)
    {
        if (isset($name)) {
            $this->name = $name;
            return $this;
        }
        return $this->name;
    }
    public function model($model = null)
    {
        if (isset($model)) {
            $this->model = $model;
            return $this;
        }
        return $this->model;
    }
    public function type($type = null)
    {
        if (isset($type)) {
            $this->type = $type;
            return $this;
        }
        return $this->type;
    }
    public function group($group = null)
    {

        if (isset($group)) {
            $this->group = $group;
            return $this;
        }
        return $this->group;
    }
    public function placeholder($placeholder = null)
    {
        if (isset($placeholder)) {

            $this->placeholder = $placeholder;
            return $this;
        }
        return $this->placeholder;
    }
    public function required($required = null)
    {
        if (isset($required)) {
            $this->required = $required;
            return $this;
        }
        return $this->required;
    }
}
