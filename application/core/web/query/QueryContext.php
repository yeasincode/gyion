<?php
namespace gyion\core\web\query;
use gyion\core\db\Q;

/**
 * 查询上下文
 */
class QueryContext
{
    //查询组件
    private $queryComponent;
    //查询对象
    private $queryModel;

    public function __construct(Q $queryModel=null,Query $queryComponent){
        $this->queryModel=$queryModel;
        $this->queryComponent=$queryComponent;
    }

    /**
     * 查询组件
     */
    public function component($queryComponent=null){
        if(isset($queryComponent))
        {
            $this->queryComponent=$queryComponent;
            return $this;
        }
        return $this->queryComponent;
    }

    /**
     * 查询对象
     */
    public function model($queryModel=null){
        if(isset($queryModel))
        {
            $this->queryModel=$queryModel;
            return $this;
        }
        return $this->queryModel;
    }

    /**
     * 返回映射
     */
    public function map()
    {
        $map=[];
        foreach($this->queryComponent->items() as $item)
        {
            $map[$item->name()]=[
                'component'=>$item,
                'field'=>null
            ];
        }

        if(!isset($this->queryModel))
        {
            return $map;
        }

        foreach($this->queryModel->fields() as $field){
            $map[$field->fieldName()]['field']=$field;
        }

        return $map;
    }
}