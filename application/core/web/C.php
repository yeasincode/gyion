<?php
namespace gyion\core\web;

use gyion\core\db\R;
use think\Controller;
use think\Session;
use think\Request;


/**
 * 控制器基类
 * author yeasin
 */
class C extends Controller
{
    protected $beforeActionList = [
        'checkLogin',
    ];

    /**
     * 实例缓存
     */
    static $_cache = [];

    /**
     * 初始化操作
     * @access protected
     */
    protected function _initialize()
    {
        parent::_initialize();
        //动态方法注入
        Request::hook('queryInfo','queryInfo'); 
        Request::hook('pageInfo','pageInfo'); 
    }

    /**
     * S{Service}方法生成
     */
    public function __call($funcName, $arguments)
    {
        $type = $funcName[0];
        switch ($type) {
            case 'S':
                return $this->serviceInstance($funcName);
        }

        return null;
    }
    /**
     * 服务列表
     */
    protected function services()
    {
        return [];
    }

    protected function checkLogin()
    {
        $request = $this->request;
        if (strcasecmp($request->controller(), 'login') == 0) {
            return;
        }
        if (strcmp($request->module(), 'platform') == 0) {
            $user = Session::get('user');
            if (!isset($user)) {
                $this->redirect('login/index');
            }
        }
    }

    /**
     * 服务实例
     */
    private function serviceInstance($funcName)
    {
        foreach ($this->services() as $name) {
            $_name = ucfirst(strtolower($name));
            $serviceName = 'S' . $_name;
            $instance = null;
            if (array_key_exists($serviceName, R::$_cache)) {
                $instance = R::$_cache[$serviceName];
                return $instance;
            }
            if (strcmp($serviceName, $funcName) == 0) {
                include APP_SERVICE_PATH . $_name . 'Service' . EXT;
                $class = 'gyion\\service\\' . $_name . 'Service';
                $instance = new $class();
                R::$_cache[$serviceName] = $instance;
                return $instance;
            }
        }
        return null;
    }

    /**
     * 访问页面
     */
    protected function retView(){
        if ($this->request->isGet()) {
            view()->send();
        }
    }

}
