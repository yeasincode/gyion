<?php
namespace gyion\core\web;

use think\response\View;
use think\Request;

class V extends View
{

    public function __construct($data = '', $code = 200, array $header = [], $options = []){
        parent::__construct($data,$code,$header,$options);

        $request = Request::instance();
        $base    = $request->root();
        $root    = strpos($base, '.') ? ltrim(dirname($base), DS) : $base;
        if ('' != $root) {
            $root = '/' . ltrim($root, '/');
            $root=\preg_replace('/public/','',$root);
        }
        $this->replace=array_merge($this->replace,[
            '__RESOURCE__'=>$root.'resource',
            '__RJS__'=>$root.'resource/js',
            '__RCSS__'=>$root.'resource/css',
            '__RIMAGE'=>$root.'resource/image',
            '__RTPL'=>$root.'resource/tpl',
            '__MRESOURCE__'=>$root.'application/'.$request->module().'/resource',
            '__MJS__'=>$root.'application/'.$request->module().'/resource/js',
            '__MCSS__'=>$root.'application/'.$request->module().'/resource/css',
            '__MIMAGE__'=>$root.'application/'.$request->module().'/resource/image',
            '__MURL__'=>$request->baseUrl().'/'.$request->module()
        ]);
    }
}
