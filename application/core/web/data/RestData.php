<?php
namespace gyion\core\web\data;

/**
 * Rest请求返回数据
 */
class RestData
{
    /**
     * 提示消息
     */
    public $msg;
    /**
     * 结果数据
     */
    public $data;
    /**
     * 是否为单行数据
     */
    public $single = false;
    /**
     * 是否请求成功
     */
    public $success;
    /**
     * 返回时间
     */
    public $timestamp;

    public function __construct($data = [],$msg='',$success=true)
    {
        $this->single=!is_array($data);
        $this->msg = $msg;
        $this->data = $data;
        $this->success=$success;
        $this->timestamp=time();
    }

    /**
     * 返回结果
     */
    public function result()
    {
        return $this->data;
    }

    /**
     * 消息
     */
    public function msg()
    {
        return $this->msg;
    }

    /**
     * 是否为单行数据
     */
    public function single()
    {
        return $this->single;
    }
    /**
     * 是否请求成功
     */
    public function success()
    {
        return $this->success;
    }

    /**
     * 当前时间
     */
    public function timestamp(){
        return $this->timestamp;
    }

}
