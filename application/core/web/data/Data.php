<?php
namespace gyion\core\web\data;

/**
 * 页面处理返回数据 
 */
class Data
{
    /**
     * 提示消息
     */
    protected $msg;
    /**
     * 结果数据
     */
    protected $data;


    public function __construct($msg,$data=[])
    {
        $this->msg=$msg;
        $this->data=$data;
    }

    /**
     * 返回结果
     */
    public function result()
    {
        return $this->data;
    }

    /**
     * 消息
     */
    public function msg(){
        return $this->msg;
    }
}