<?php
namespace gyion\core\web\data;

use gyion\core\web\query\QueryContext;

/**
 * 视图数据
 */
class ViewData
{
    //空操作
    public static $status_none = 'none';
    //操作成功
    public static $status_success = 'success';
    //操作失败
    public static $status_error = 'error';

    //分页对象
    protected $pageInfo;

    //查询对象
    protected $queryInfo;

    //查询组件
    protected $queryComponent;

    //数据
    protected $data;

    //状态
    protected $status;

    //查询上下文
    protected $queryContext;

    public function __construct($data = null, $queryInfo = null, $pageInfo = null, $queryComponent = null, $status = 'none')
    {
        $this->data = $data;
        $this->status = $status;
        $this->queryInfo = $queryInfo;
        $this->pageInfo = $pageInfo;
        $module = \request()->module();
        $queryClass = isset($queryComponent) ? 'gyion\\' . $module . '\\query\\' . $queryComponent : 'gyion\core\web\query\Query';
        $this->queryComponent = new $queryClass();
        $this->queryComponent->init();
        $this->queryContext = new QueryContext($this->queryInfo, $this->queryComponent);
    }

    /**
     * 状态
     */
    public function data($data = null)
    {
        if (isset($data)) {
            $this->data = $data;
        }
        return $this->data;
    }

    /**
     * 分页对象
     */
    public function pageInfo($pageInfo = null)
    {
        if (isset($pageInfo)) {
            $this->pageInfo = $pageInfo;
        }
        return $this->pageInfo;

    }

    /**
     * 查询上下文
     */
    public function queryContext($queryContext = null)
    {
        if (isset($queryContext)) {
            $this->queryContext = $queryContext;
        }
        return $this->queryContext;
    }

    /**
     * 状态
     */
    public function status($status = null)
    {
        if (isset($status)) {
            $this->status = $status;
        }
        return $this->status;
    }
    /**
     * 空操作
     */
    public static function none($data = null)
    {
        $viewData = new self($data, self::$status_none);
        return $viewData;
    }
    /**
     * 成功
     */
    public static function success($msg, $data = null)
    {
        $success = new Success($msg, $data);
        $viewData = new self($success, self::$status_success);
        return $viewData;
    }

    /**
     * 错误
     */
    public static function error($msg, $data = null)
    {
        $error = new Error($msg, $data);
        $viewData = new self($error, self::$status_error);
        return $viewData;
    }

}
