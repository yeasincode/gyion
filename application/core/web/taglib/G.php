<?php
namespace gyion\core\web\taglib;

use think\template\TagLib;

/**
 * 标签库
 */
class G extends TagLib
{
    /**
     * 定义标签列表
     */
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'query' => ['attr' => 'model', 'close' => 0],
        'page' => ['attr' => 'model,url', 'close' => 0],
    ];

    public function tagQuery($tag, $content)
    {
        $queryContext = $tag['model'];
        $parse = '<form method="post" class="layui-form layui-col-md12 x-so">';
        $parse .= '<?php ';
        $parse .= '$__map=' . $queryContext . '->map();';
        $parse .= 'foreach($__map as $__name=>$__item){;';
        $parse .= ' $__com=$__item[\'component\'];';
        $parse .= ' $__field=$__item[\'field\'];';
        $parse .= ' if(isset($__field)){;';
        $parse .= '     $__value=$__field->fieldValue();';
        $parse .= ' if(strcmp($__field->fieldType(),\'date\')==0&&$__field->isRange()){;';
        $parse .= '     list($__value1,$__value2)=explode(\'|\',$__value);';
        $parse .= ' }}else{;';
        $parse .= '     $__value=\'\';';
        $parse .= '     $__value1=\'\';';
        $parse .= '     $__value2=\'\';';
        $parse .= ' }';
        $parse .= ' switch($__com->type()){';
        $parse .= ' case \'text\':';
        $parse .= '     echo \'<input type="text" style="margin-right: 5px;" name="\'.$__name.\'" g-type="text" g-model="\'.$__com->model().\'"  value="\'.$__value.\'" placeholder="\'.$__com->placeholder().\'" class="layui-input"/>\';';
        $parse .= ' break;';
        $parse .= ' case "date":';
        $parse .= '     if(null!==$__com->group()){';
        $parse .= '         list($placeholder1,$placeholder2)=explode(\'-\',$__com->placeholder());';
        $parse .= '         echo \'<input class="layui-input" style="margin-right: 5px;" name="\'.$__name.\'" type="text" g-type="date" g-model="\'.$__com->model().\'" value="\'.$__value1.\'" placeholder="\'.$placeholder1.\'" g-group="\'.$__com->group().\'" />\';';
        $parse .= '         echo \'<input class="layui-input" style="margin-right: 5px;" name="\'.$__name.\'" type="text" g-type="date" g-model="\'.$__com->model().\'" value="\'.$__value2.\'" placeholder="\'.$placeholder2.\'" g-group="\'.$__com->group().\'" />\';';
        $parse .= '     }else{';
        $parse .= '         echo \'<input class="layui-input" style="margin-right: 5px;" name="\'.$__name.\'" type="text" g-type="date" g-model="\'.$__com->model().\'" value="\'.$__value.\'" placeholder="\'.$__com->placeholder().\'" />\';';
        $parse .= '     }';
        $parse .= ' break;';
        $parse .= ' }';
        $parse .= '}?>';
        $parse .= '<button class="layui-btn" lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>';
        $parse .= "<input type='hidden' name='queryModel' />";
        $parse .= '</form>';
        return $parse;
    }

    /**
     * 分页
     */
    public function tagPage($tag)
    {
        $pageModel = $tag['model'];
        $url = $tag['url'];
        $parse = '<?php ';
        $parse .= '$__pageCount =\ceil(' . $pageModel . '->totalSize() / ' . $pageModel . '->pageSize());';
        $parse .= '$__current=' . $pageModel . '->current()>$__pageCount?$__pageCount:' . $pageModel . '->current();';
        $parse .= '$__prev = $__current - 1 > 0 ? ' . $pageModel . '-> current() - 1 : 1;';
        $parse .= '$__next = $__current + 1 > $__pageCount ? $__pageCount : $__current + 1;';
        $parse .= '$__segment=5;';
        $parse .= '$__start=$__current>$__segment?$__current-$__segment:1;';
        $parse .= '$__end=$__start==1?$__segment*2-$__current+1:$__current+$__segment;';
        $parse .= '$__end=$__end>$__pageCount?$__pageCount:$__end;';
        $parse .= '$__href=\'javascript:' . $url . '($index$)\';';
        $parse .= ' ?>';
        $parse .= '<div class="page">';
        $parse .= '<div>';
        $parse .= '<?php ';
        $parse .= ' echo \'<a class="prev" href="\'.str_replace(\'$index$\',$__prev,$__href).\'">&lt;&lt;</a>\';';
        $parse .= 'for (; $__start < $__current;$__start++) {';
        $parse .= 'echo \'<a class="num" href="\'.str_replace(\'$index$\',$__start,$__href).\'">\'.$__start.\'</a>\';';
        $parse .= '}';
        $parse .= 'echo \'<span class="current">\'.$__current.\'</span>\';';
        $parse .= 'for ($__j = $__current+1; $__j <= $__end; $__j++) {';
        $parse .= 'echo \'<a class="num" href="\'.str_replace(\'$index$\',$__j,$__href).\'">\'.$__j.\'</a>\';';
        $parse .= '}';

        $parse .= ' echo \'<a class="next" href="\'.str_replace(\'$index$\',$__next,$__href).\'">&gt;&gt;</a>\';';
        $parse .= '?>';
        $parse .= '</div>';
        $parse .= '</div>';
        return $parse;
    }

}
