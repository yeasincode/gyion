<?php
namespace gyion\core\db;

use gyion\core\exception\DbException;
use think\Config;
use think\Db;
use think\db\Query;

/**
 * Repository基类
 */
class R
{

    /**
     * 实例缓存
     */
    static $_cache = [];

    /**
     * T{Table},R{Repository}方法生成
     */
    public function __call($funcName, $arguments)
    {
        $type = $funcName[0];
        switch ($type) {
            case 'T':
                return $this->tableInstance($funcName);
            case 'R':
                return $this->repositoryInstance($funcName);
        }

        return null;
    }

    /**
     * 初始化实例
     */
    private function tableInstance($funcName)
    {
        $tableConfig = $this->getModelMap()['Table'];
        foreach ($this->tables() as $name) {
            $tableName = 'T' . ucfirst(strtolower($name));
            if (strcmp($tableName, $funcName) == 0) {
                $alias = $tableConfig[$name]['alias'];
                return Db::name($name)->alias($alias);
            }
        }
        return null;
    }

    /**
     * 仓储实例
     */
    private function repositoryInstance($funcName)
    {
        foreach ($this->repositorys() as $name) {
            $_name = ucfirst(strtolower($name));
            $repositoryName = 'R' . $_name;
            $instance = null;
            if (array_key_exists($repositoryName, R::$_cache)) {
                $instance = R::$_cache[$repositoryName];
                return $instance;
            }
            if (strcmp($repositoryName, $funcName) == 0) {
                include APP_REPOSITORY_PATH . $_name . 'Repository' . EXT;
                $class = 'gyion\\repository\\' . $_name . 'Repository';
                $instance = new $class();
                R::$_cache[$repositoryName] = $instance;
                return $instance;
            }
        }
        return null;
    }

    /**
     * 数据库表列表
     */
    protected function tables()
    {
        return [];
    }

    /**
     * 仓储列表
     */
    protected function repositorys()
    {
        return [];
    }

    /**
     * 获取表映射关系
     */
    protected function getModelMap()
    {
        $tableMapConfig = Config::get('ModelMap');
        return $tableMapConfig;
    }

    private function getMajor()
    {
        $tableConfig = $this->getModelMap()['Table'];
        if (empty($this->tables())) {
            throw new DbException('当前仓储未设置主表');
        }
        $majorTable = $this->tables()[0];
        $alias = $tableConfig[$majorTable]['alias'];
        $db = Db::name($majorTable)->alias($alias);
        return $db;
    }

    /**
     * 新增数据
     */
    public function insert($data)
    {
        $db = $this->getMajor();
        $db->insert($data)->getLastInsID();
    }

    /**
     * 更新数据
     */
    public function update($id, $data)
    {
        $db = $this->getMajor();
        $db->where('id', $id)->update($data);
    }

    /**
     * 删除数据
     */
    public function delete($id)
    {
        $db = $this->getMajor();
        $db->where('id', $id)->delete();
    }

    /**
     * 获取数据
     */
    public function get($id)
    {
        $db = $this->getMajor();
        $db->where('id', $id)->find();
    }

    /**
     * 展开查询数据
     */
    protected function expandQueryInfo(Query $db, Q $queryInfo = null)
    {
        // Db::listen(function ($sql, $time, $explain, $master) {
        //     // 记录SQL
        //     echo $sql . ' [' . $time . 's] ' . ($master ? 'master' : 'slave');
        //     // 查看性能分析结果
        //     dump($explain);
        // });

        if (!isset($queryInfo)) {
            return;
        }

        $majorTable = $this->tables()[0];

        $tableQ = [$majorTable];

        $modelMap = $this->getModelMap();
        $tableConfig = $modelMap['Table'];
        $tableRetation = $modelMap['Relation'];
        $marjorAlias = $tableConfig[$majorTable]['alias'];
        $db->alias($marjorAlias);

        foreach ($queryInfo->fields() as $field) {
            if (!in_array($field->modelName(), $tableQ)) {
                foreach ($tableQ as $value) {
                    $key = \hash2key($value, $field->modelName());
                    if (!array_key_exists($key, $tableRetation)) {
                        throw new DbException('当前表{' . $field->modelName() . '}不符合查询条件');
                    }
                    $tableMap = $tableRetation[$key];
                    $alias = $tableConfig[$field->modelName()]['alias'];
                    $tableName = $field->modelName() . ' ' . $alias;
                    $db->join($tableName, $tableMap['join']);
                    $this->fieldWhere($db, $field, $alias);
                }
            } else {
                $alias = $tableConfig[$field->modelName()]['alias'];
                $this->fieldWhere($db, $field, $alias);
            }
        }
    }

    /**
     * Field where条件生成
     */
    private function fieldWhere(Query $db, $field, $alias)
    {
        if (\strcmp($field->fieldType(), 'date') == 0) {
            if (null !== $field->startValue()) {
                $db->whereTime($alias . '.' . $field->fieldName(), '>=', $field->startValue());
            }

            if (null !== $field->endValue()) {
                $db->whereTime($alias . '.' . $field->fieldName(), '<=', $field->endValue());
            }
        } else {
            $db->where($alias . '.' . $field->fieldName(), $field->fieldValue());
        }
    }

    /**
     * 根据查询对象查询数据
     */
    function list(Q $queryInfo = null, P $pageInfo = null) {
        $db = $this->getMajor();

        if (isset($pageInfo)) {
            $this->expandQueryInfo($db, $queryInfo);
            $count = $db->count();
            $pageInfo->totalSize($count);
            $db->page($pageInfo->current(),
                $pageInfo->pageSize())->order($pageInfo->orders());
        }

        $this->expandQueryInfo($db, $queryInfo);
        $list = $db->select();
        return $list;
    }
}
