<?php
namespace gyion\core\db;

/**
 * 字段
 */
class Field
{
    /**
     * 模型名
     */
    protected $modelName;

    /**
     * 字段类型 text,datetime,number
     */
    protected $fieldType;

    /**
     * 字段名
     */
    protected $fieldName;

    /**
     * 字段值
     */
    protected $fieldValue;

    public function __construct($fieldName, $fieldValue, $fieldType, $modelName)
    {
        $this->fieldName = $fieldName;
        $this->fieldValue = $fieldValue;
        $this->fieldType = $fieldType;
        $this->modelName = $modelName;
    }

    /**
     * 字段名
     */
    public function fieldName()
    {
        return $this->fieldName;
    }
    /**
     * 字段值
     */
    public function fieldValue()
    {
        return $this->fieldValue;
    }
    /**
     * 字段类型
     */
    public function fieldType()
    {
        return $this->fieldType;
    }
    /**
     * 模型名
     */
    public function modelName()
    {
        return $this->modelName;
    }
}
