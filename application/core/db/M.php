<?php
namespace gyion\core\db;

/**
 * 模型基类
 */
class M
{
    /**
     * 创建用户
     */
    public $create_user;
    /**
     * 创建用户名
     */
    public $create_username;
    /**
     * 创建时间
     */
    public $create_time;
    /**
     * 修改用户
     */
    public $modify_user;
    /**
     * 修改用户名
     */
    public $modify_username;
    /**
     * 修改时间
     */
    public $modify_time;


}