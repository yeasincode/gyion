<?php
namespace gyion\core\db;

/**
 * 分页对象
 */
class P
{
    //当前所在页面
    private $current;
    //每页个数
    private $pageSize;
    //总数
    private $totalSize;
    //排序
    private $orders;

    public function __construct()
    {
        $this->current = 1;
        $this->pageSize = 10;
        $this->totalSize = 0;
        $this->orders = ['create_time' => 'desc'];
    }

    /**
     *当前所在页面
     */
    public function current($current=null)
    {
        if (isset($current)) {
            $this->current = $current;
            return $this;
        }
        return $this->current;
    }
    /**
     *每页个数
     */
    public function pageSize($pageSize=null)
    {
        if (isset($pageSize)) {
            $this->pageSize = $pageSize;
            return $this;
        }
        return $this->pageSize;
    }
    /**
     *总数
     */
    public function totalSize($totalSize=null)
    {
        if (isset($totalSize)) {
            $this->totalSize = $totalSize;
            return $this;
        }
        return $this->totalSize;
    }
    /**
     *排序
     */
    public function orders($orders=null)
    {
        if (isset($orders)) {
            $this->orders= $orders;
            return $this;
        }
        return $this->orders;
    }
}
