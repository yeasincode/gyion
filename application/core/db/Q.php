<?php
namespace gyion\core\db;

/**
 * 查询对象
 */
class Q
{
    //查询字符
    protected $queryStr;

    //查询字段
    protected $fields;

    public function __construct($fields = [], $queryStr='')
    {
        $this->queryStr = $queryStr;
        $this->fields = $fields;
    }

    public function fields()
    {
        return $this->fields;
    }

    public function queryStr()
    {
        return $this->queryStr;
    }

}
