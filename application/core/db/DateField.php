<?php
namespace gyion\core\db;

/**
 * 日期字段
 */
class DateField extends Field
{

    /**
     * 是否表示范围
     */
    protected $isRange;

    /**
     * 起始值
     */
    protected $startValue;

    /**
     * 结束值
     */
    protected $endValue; 

    public function __construct($fieldName,$fieldType, $modelName,$startValue,$endValue=null)
    {
        $this->fieldName = $fieldName;
        $this->isRange=isset($endValue);
        $this->startValue=$startValue;
        $this->endValue=$endValue;
        $this->fieldValue=($this->isRange)?$startValue.'|'.$endValue:$startValue;
        $this->fieldType = $fieldType;
        $this->modelName = $modelName;
    }


    /**
     * 是不表示范围
     */
    public function isRange()
    {
        return $this->isRange;
    }
    /**
     * 起始取值
     */
    public function startValue()
    {
        return $this->startValue;
    }
    /**
     * 结束取值
     */
    public function endValue()
    {
        return $this->endValue;
    }

}