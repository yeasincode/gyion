<?php
namespace gyion\core\db;

/**
 * 表和表的映射关系
 */
class TableMap
{
    /**
     * 表1
     */
    protected $table1;
    /**
     * 表2
     */
    protected $table2;
    /**
     * 连接类型
     */
    protected $joinType;

    /**
     * 连接
     */
    protected $join = '';

    public function __construct($table1, $table2, $joinField1 = 'id', $joinField2 = 'id', $joinType = 'inner join')
    {
        $this->table1 = $table1;
        $this->table2 = $table2;
        $this->joinField1 = $joinField1;
        $this->joinField2 = $joinField2;
        $this->join = '$t1.'
        . $this->joinField1 . '='
        . '$t2.'
        . $this->joinField2;
        $this->joinType = $joinType;
    }

    /**
     * 表1
     */
    public function table1()
    {
        return $this->table1;
    }
    /**
     * 表2
     */
    public function table2()
    {
        return $this->table2;
    }
    /**
     * 连接类型
     */
    public function joinType()
    {
        return $this->joinType;
    }

    /**
     * 设置连接
     */
    public function join($join=null)
    {
        if (isset($join)) {
            $this->join = $join;
            return $this;
        }

        $tableJoin=\str_replace('$t1',$this->table1,$this->join);
        $tableJoin=\str_replace('$t2',$this->table2,$tableJoin);

        return $tableJoin;
    }

}
