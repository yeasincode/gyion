<?php 
 // +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
/**
 * 对象转数组
 * @param $obj 对象
 */
function object_array($obj)
{
    $result=[];
    $arr=is_object($obj)?get_object_vars($obj):$obj;
    foreach($arr as $key =>$value)
    {
        $value=(is_array($value)||is_object($value)?object_array($value):$value);
        $result[$key]=$value;
    }
    return $result;
}

/**
 * 数组转对象
 * @param $arr 数组
 * @param $class 类型
 */
function array_object($arr,$class){

  if(is_array($arr)){
      $obj=new $class();
      foreach($arr as $key=>$value)
      {
        $obj->$key($value);
      }
      return $obj;
  }
  return $arr;
}
