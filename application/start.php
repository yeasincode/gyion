<?php
require __DIR__.'/../thinkphp/library/think/App.php';
// 1. 加载基础文件
require __DIR__ . '/../thinkphp/base.php';
// 2. 加载应用基础文件
require 'base.php';
// 3.加载Overwrite autoload.php文件
require __DIR__.'/../overwrite/autoload.php';
// 4. 执行应用
$response=\think\App::run();
// 5.发送结果
$response->send();