<?php
return [
    /**请求相关 */
    'request_success'=>'The request completed successfully!',
    'request_faild'=>'The request failed!',
    /**列表相关 */
    'total'=>'total',
    'edit'=>'edit',
    'delete'=>'delete',
    'detail'=>'detail',
    'get'=>'get',
    'query'=>'query',

];