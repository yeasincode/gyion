<?php
return [
    /**请求相关 */
    'request_success'=>'请求成功！',
    'request_faild'=>'请求失败！',
    /**列表相关 */
    'total'=>'总共',
    'edit'=>'编辑',
    'delete'=>'删除',
    'detail'=>'详情',
    'get'=>'获取',
    'query'=>'查询',
];