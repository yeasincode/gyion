<?php
namespace gyion\repository;

use gyion\core\db\R;

/**
 * 用户仓储
 */
class UserRepository extends R
{
    /**
     * 设置表名
     */
    protected function tables()
    {
        return ['user'];
    }

    /**
     * 根据用户名查询用户
     */
    public function getByName($username)
    {
        return $this->TUser()->where('username', $username)->find();
    }
}
