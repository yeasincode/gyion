<?php
namespace gyion\repository;

use gyion\core\db\R;

/**
 * 角色仓储
 */
class RoleRepository extends R
{
    /**
     * 设置表名
     */
    protected function tables()
    {
        return ['role'];
    }
}