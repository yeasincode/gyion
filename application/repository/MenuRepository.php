<?php
namespace gyion\repository;
use gyion\core\db\R;
/**
 * 菜单仓储
 */
class MenuRepository extends R
{
    /**
     * 设置表名
     */
    protected function tables(){
        return ['menu'];
    }


    /**
     * 查询菜单列表
     */
    public function getMenuList(){
        $this->TMenu()->select();
    }
}