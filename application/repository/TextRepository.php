<?php
namespace gyion\repository;

use gyion\core\db\R;

/**
 * 文本仓储
 */
class TextRepository extends R
{
    /**
     * 设置表名
     */
    protected function tables()
    {
        return ['text'];
    }

}
